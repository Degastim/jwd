package com.company;
import java.util.Scanner;

import static java.lang.Math.sqrt;
public class Main {

    public static void main(String[] args)
    {
        double X,Y,Z,T;
        Scanner scanner=new Scanner(System.in);
        X=scanner.nextDouble();
        Y=scanner.nextDouble();
        Z=scanner.nextDouble();
        T=scanner.nextDouble();
        System.out.print(square(X,Y,Z,T));
    }
    static double square(double x,double y,double z,double t)
    {
        double p=(x+y+z+t)/2;
        return (sqrt((p-x)*(p-y)*(p-z)*(p-t)));
    }
}

