package com.company;
import java.util.Scanner;

import static java.lang.Math.sqrt;
public class Main {

    public static void main(String[] args)
    {
        double a,b,c;
        Scanner scanner=new Scanner(System.in);
        a=scanner.nextDouble();
        b=scanner.nextDouble();
        c=scanner.nextDouble();
        System.out.print(square(a,b,c));
    }

    public static double square(double a,double b,double c)
    {
        double p=(a+b+c)/2;
        return (sqrt(p*(p-a)*(p-b)*(p-c)));
    }
}
