package com.company;
import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
	int n;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        double[][] array=new double[n][2];
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<2;j++)
            {
                array[i][j]=scanner.nextDouble();
            }
        }
        length(n,array);
    }
    public static void length(int n,double[][] array)
    {
        double max[][]=new double[2][2];
        double maxlength=0;
        for (int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
        {
            if(sqrt(pow(abs(array[i][0]-array[j][0]),2)+ pow(abs(array[i][1]-array[j][1]),2))>maxlength)
            {
                maxlength=sqrt(pow(abs(array[i][0]),2)+ pow(abs(array[j][0]),2));
                max[0][0]=array[i][0];
                max[0][1]=array[i][1];
                max[1][0]=array[j][0];
                max[1][1]=array[j][0];
            }

        }

    }
        System.out.print(max[0][0]+" "+max[0][1]+"\n"+max[1][0]+" "+max[1][1]);
    }
}
