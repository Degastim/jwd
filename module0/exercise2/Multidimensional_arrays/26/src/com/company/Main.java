package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	int n,m,imax,imin;
	double min,max,t;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        m=scanner.nextInt();
        double a[][]=new double[n][m];
        double b[][]=new double[2][n];
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                a[i][j]=scanner.nextDouble();
            }
        }
        for (int j=0;j<n;j++)
        {
            b[1][j]=a[j][0];
        }
        for (int i=0;i<n;i++)
        {
            max=a[i][0];
            min=a[i][0];
            imax=0;
            imin=0;
            for (int j=0;j<m;j++)
            {
                if(a[i][j]<0) b[0][i]+=a[i][j];
                if(b[1][i]<a[i][j]) b[1][i]=a[i][j];
                if (a[i][j]>max)
                {
                    max=a[i][j];
                    imax=j;
                }
                if (a[i][j]<min)
                {
                    min=a[i][j];
                    imin=j;
                }
            }
            t=a[i][imin];
            a[i][imin]=a[i][imax];
            a[i][imax]=t;
        }
        for (int i=0;i<n;i++)
        {

            for (int j=0;j<m;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        for (int i=0;i<2;i++)
        {
            if(i==0) System.out.print("Сумма отрицательных чисел ");
            else System.out.print("Максимальные числа ");
            for (int j=0;j<n;j++)
            {
                System.out.print(b[i][j]+" ");
            }
            System.out.println();
        }
    }
}
