package com.company;

import java.util.Scanner;
import java.util.ServiceConfigurationError;

public class Main {

    public static void main(String[] args)
    {
        int n;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        double a[][]=new double[n][n];
        for(int i=0;i<n;i++)
        {
            a[0][i]=scanner.nextDouble();
        }
        for(int i=1;i<n;i++)
        {
           for (int j=0;j<n;j++)
           {
               a[i][j]=a[i-1][j]*a[0][j];
           }
        }
        for(int i=0;i<n;i++)
        {
            for (int j=0;j<n;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}
