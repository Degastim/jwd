package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int n,m;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        m=scanner.nextInt();
        double a[][]=new double[n][m];
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                a[i][j]=scanner.nextDouble();
            }
        }
        if(n!=m)
        {
            System.out.print("Неверное количество столбцов и строк");
            System.exit(0);
        }
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                if(i==j) System.out.print(a[i][j]+" ");
            }
        }
    }
}
