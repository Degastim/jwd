﻿package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        int N,M;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Введите количество строк");
        N=scanner.nextInt();
        System.out.println("Введите количество столбцов");
        M=scanner.nextInt();
        int a[][]=new int[N][M];

        for(int i=0;i<N;i++)
        {
            for (int j=0;j<M;j++)
            {
                a[i][j]=scanner.nextInt();
            }
        }

        for(int i=0;i<N;i++)
        {
            for (int j=0;j<M;j++)
            {
                if(i==0||i==(N-1))
                {
                    System.out.print(a[i][j]+" ");
                }
            }
            System.out.println();
        }
    }
}
