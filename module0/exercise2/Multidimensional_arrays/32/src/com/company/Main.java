package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        int n,m,t;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        m=scanner.nextInt();
        int a[][]=new int[n][m];
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                a[i][j]=scanner.nextInt();
            }
        }

        for (int i=0;i<n;i++)
        {
            for (int k=0;k<m;k++)
            {
                for (int j = 1; j < m; j++)
                {
                    if(a[i][j-1]>a[i][j])
                    {
                        t=a[i][j-1];
                        a[i][j-1]=a[i][j];
                        a[i][j]=t;
                    }
                }
            }
        }
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i=0;i<n;i++)
        {
            for (int k=0;k<m;k++)
            {
                for (int j = 0; j < m-1; j++)
                {
                    if(a[i][j]<a[i][j+1])
                    {
                        t=a[i][j+1];
                        a[i][j+1]=a[i][j];
                        a[i][j]=t;
                    }
                }
            }
        }
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}
