package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        int n;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        int a[][]=new int[n][n];
        for (int i=0;i<n/2;i++)
        {
            for (int j = 0; j < n; j++)
            {
                a[i][j] = 1;
                if ( j < i) a[i][j] = 0;
                if (n-i<=j) a[i][j] = 0;
            }
        }
        for (int i=n-1;i>=n/2;i--)
        {
            for (int j = 0; j < n; j++)
            {
                a[i][j] = 1;
                if (n-1-j > i) a[i][j] = 0;
                if(j>i)  a[i][j] = 0;
            }
        }

        for (int i=0;i<n;i++)
        {
            for (int j = 0; j < n; j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}
