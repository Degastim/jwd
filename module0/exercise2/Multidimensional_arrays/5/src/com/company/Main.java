package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	int N,M;
        Scanner scanner=new Scanner(System.in);
        N=scanner.nextInt();
        M=scanner.nextInt();
        int a[][]=new int[N][M];
        for (int i=0;i<N;i++)
        {
            for (int j=0;j<M;j++)
            {
               a[i][j]=scanner.nextInt();
            }
        }

        for (int i=0;i<N;i++)
        {
            for (int j=0;j<M;j++)
            {
                if ((i+1)%2==0) System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}
