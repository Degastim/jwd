package com.company;

import java.util.Scanner;
import static java.lang.Math.sin;
import static java.lang.Math.pow;
public class Main {

    public static void main(String[] args)
    {
        int n,num=0;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        double a[][]=new double[n][n];
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<n;j++)
            {
                a[i][j]=sin((pow(i,2)-pow(j,2))/n);
                if(a[i][j]>0) num++;
            }
        }
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<n;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println(num);
    }
}
