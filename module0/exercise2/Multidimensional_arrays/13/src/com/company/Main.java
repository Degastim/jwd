package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int n;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        int a[][]=new int[n][n];
        for (int i=0;i<n;i++)
        {
            if ((i+1)%2!=0)
            {

                for (int j=0;j<n;j++)
                {
                    a[i][j]=j+1;
                    System.out.print(a[i][j]+" ");
                }
            }
            else
            {
                for (int j=n-1;j>=0;j--)
                {
                    a[i][j]=j+1;
                    System.out.print(a[i][j]+" ");
                }
            }
            System.out.println();
        }
    }
}
