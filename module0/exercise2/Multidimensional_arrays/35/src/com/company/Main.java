package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int n,m,max;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        m=scanner.nextInt();
        int a[][]=new int[n][m];
        a[0][0]=scanner.nextInt();
        max=a[0][0];
        for (int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                if(i!=0||j!=0)
                {
                    a[i][j] = scanner.nextInt();
                    if(a[i][j]>max) max=a[i][j];
                }
            }
        }
        for (int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                if(a[i][j]%2!=0) a[i][j]=max;
            }
        }
        for (int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
               System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}
