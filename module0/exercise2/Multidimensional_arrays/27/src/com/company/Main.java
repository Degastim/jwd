package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        int n,m,num1,num2;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        m=scanner.nextInt();
        num1=scanner.nextInt();
        num2=scanner.nextInt();
        double t;
        double a[][]=new double[n][m];
        double b[]=new double[m];
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                a[i][j]=scanner.nextDouble();
            }
        }
        for (int i=0;i<n;i++)
        {
           t=a[i][num1-1];
           a[i][num1-1]=a[i][num2-1];
           a[i][num2-1]=t;
        }
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
               System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }

    }
}
