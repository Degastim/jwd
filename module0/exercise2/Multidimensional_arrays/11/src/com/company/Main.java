package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        int n,m;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        m=scanner.nextInt();
        double a[][]=new double[n][m];
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                a[i][j]=scanner.nextDouble();
            }
        }
        for (int i=0;i<n;i++)
        {
           if ((i+1)%2!=0)
           {
               for (int j=m-1;j>=0;j--)
               {
                   System.out.print(a[i][j]+" ");
               }
           }
           else
           {
               for (int j=0;j<m;j++)
               {
                   System.out.print(a[i][j]+" ");
               }
           }
           System.out.println();
        }
    }
}
