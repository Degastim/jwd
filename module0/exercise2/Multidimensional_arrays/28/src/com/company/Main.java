package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	int n,m;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        m=scanner.nextInt();
        double max=0;
        double a[][]=new double[n][m];
        double b[]=new double[m];
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                a[i][j]=scanner.nextDouble();
            }
        }
        for (int i=0;i<m;i++)
        {
            for (int j=0;j<n;j++)
            {
                b[i]+=a[j][i];
            }
        }
        for (int i=0;i<m;i++)
        {
            if(max<b[i]) max=b[i];
        }
        for (int i=0;i<m;i++)
        {
               System.out.print(b[i]+" ");
        }
        System.out.print("\n"+max);
    }
}
