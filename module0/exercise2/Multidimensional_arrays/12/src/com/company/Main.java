package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	int n;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        int a[][]=new int[n][n];
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<n;j++)
            {
                a[i][j]=0;
                if(i==j) a[i][j]=i;
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}
