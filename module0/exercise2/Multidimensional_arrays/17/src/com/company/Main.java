package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int n;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        int a[][]=new int[n][n];
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<n;j++)
            {
                if(i==0||j==0||i==n-1||j==n-1)a[i][j]=1;
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}