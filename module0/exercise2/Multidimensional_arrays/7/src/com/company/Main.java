package com.company;

import java.util.Scanner;
import static java.lang.Math.abs;
public class Main {

    public static void main(String[] args)
    {
        double sum=0;
    double a[][]=new double[5][5];
    Scanner scanner=new Scanner(System.in);
    for(int i=0;i<5;i++)
    {
        for (int j=0;j<5;j++)
        {
            a[i][j]=scanner.nextDouble();
            if(a[i][j]<0&&a[i][j]%2!=0) sum+=abs(a[i][j]);
        }
    }
    System.out.println(sum);
    }
}
