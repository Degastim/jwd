package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	int n;
        Scanner scanner=new Scanner(System.in);
        n=scanner.nextInt();
        double a[][]=new double[n][n];
        double b[]=new double[n];
        for (int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++)
            {
                a[i][j]=scanner.nextDouble();
                if(i+j+1==n&&a[i][j]>0) b[i]=a[i][j];
            }
        }
        for (int i=0;i<n;i++)
        {
              System.out.print(b[i]+" ");
        }
    }
}
