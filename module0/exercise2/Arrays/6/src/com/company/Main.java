package com.company;

import java.util.Scanner;
import static java.lang.Math.abs;
public class Main {

    public static void main(String[] args)
    {
       int N,max,min;
        Scanner scanner=new Scanner(System.in);
        N=scanner.nextInt();
        int a[]=new int[N];
        a[0]=scanner.nextInt();
        max=a[0];
        min=a[0];
        for (int i=1;i<N;i++)
        {
            a[i]=scanner.nextInt();
            if (a[i]>max) max=a[i];
            if (a[i]<min) min=a[i];
        }
        System.out.print(max+abs(min));
    }
}
