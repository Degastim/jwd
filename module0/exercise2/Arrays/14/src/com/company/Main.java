package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        int N,k,max,min;
        Scanner scanner=new Scanner(System.in);
        N=scanner.nextInt();
        k=scanner.nextInt();
        int a[]=new int[N];
        a[0]=scanner.nextInt();
        min=a[0];
        a[1]=scanner.nextInt();
        max=a[1];
        for (int i=2;i<N;i++)
        {
            a[i]=scanner.nextInt();
            if((i+1)%2==0||a[i]>max) max=a[i];
            if((i+1)%2!=0||a[i]<min) min=a[i];
        }
        System.out.print(max+min);
    }
}
