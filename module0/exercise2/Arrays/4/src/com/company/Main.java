package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
	int N;
	boolean in=true;
        Scanner scanner=new Scanner(System.in);
        N=scanner.nextInt();
        int a[]=new int[N];
        for (int i=0;i<N;i++) a[i]=scanner.nextInt();
        for (int i=1;i<N;i++)
        {
            if (a[i]<=a[i-1]) in=false;
        }
        System.out.print(in);
    }
}
