package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        int N,M,L,sum=0;
        Scanner scanner=new Scanner(System.in);
        N=scanner.nextInt();
        M=scanner.nextInt();
        L=scanner.nextInt();
        int a[]=new int[N];
        for (int i=0;i<N;i++)
        {
            a[i]=scanner.nextInt();
            if(i>L||a[i]%M==0) sum++;
        }
        System.out.print(sum);
    }
}
