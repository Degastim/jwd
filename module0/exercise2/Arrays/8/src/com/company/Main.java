package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        int N,positive=0,negative=0,zero=0;
        Scanner scanner=new Scanner(System.in);
        N=scanner.nextInt();
        int a[]=new int[N];
        for (int i=0;i<N;i++)
        {
            a[i]=scanner.nextInt();
            if (a[i]>0) positive++;
            if (a[i]<0) negative++;
            if (a[i]==0) zero++;
        }
        System.out.print(positive+" "+negative+" "+zero);
    }
}
