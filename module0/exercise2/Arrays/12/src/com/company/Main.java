package com.company;

import java.util.Scanner;
import static java.lang.Math.sqrt;
public class Main {

    public static void main(String[] args)
    {
        int N;
        double sum=0;
        boolean b=true;
        Scanner scanner=new Scanner(System.in);
        N=scanner.nextInt();
        double a[]=new double[N];
        for (int i=0;i<N;i++)
        {
            a[i]=scanner.nextDouble();
            if(a[i]==1) b=false;
            if(a[i]==2) b=true;
            for (int j=2;j<=sqrt(a[i]);j++)
            {
                if(a[i]%j==0) b=false;
            }
            if(b) sum+=a[i];
            b=true;
        }
        System.out.print(sum);
    }
}
