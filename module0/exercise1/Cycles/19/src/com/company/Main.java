package com.company;
import static java.lang.Math.pow;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        double e;
        double sum=0;
        Scanner scanner=new Scanner(System.in);
        e=scanner.nextDouble();
        for (double i=1;e<=(1/pow(2,i)+1/pow(3,i));i++)
        {
            sum+=1/pow(2,i)+1/pow(3,i);
        }
        System.out.println(sum);
    }
}
