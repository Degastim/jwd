package com.company;

import java.util.Scanner;
import static java.lang.Math.pow;
public class Main {

    public static void main(String[] args) {
	double x,F=0;
        Scanner scanner=new Scanner(System.in);
        x=scanner.nextDouble();
        if(x>=3)F=-pow(x,2)+3*x+9;
        if (x<3)F=1/(pow(x,3)-6);
        System.out.println(F);
    }
}
