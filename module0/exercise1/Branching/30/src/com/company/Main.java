package com.company;

import java.util.Scanner;
import static java.lang.Math.abs;
public class Main {

    public static void main(String[] args)
    {
	double a,b,c;
        Scanner scanner=new Scanner(System.in);
        a=scanner.nextDouble();
        b=scanner.nextDouble();
        c=scanner.nextDouble();
        if((a>b)&&(b>c))
        {
            a*=2;
            b*=2;
            c*=2;
        } else
        {
            a=abs(a);
            b=abs(b);
            c=abs(c);
        }
        System.out.println(a+" "+b+" "+c);
    }
}
