package com.company;

import java.util.Scanner;
import static java.lang.Math.pow;
public class Main {

    public static void main(String[] args) {
        double x,F=0;
        Scanner scanner=new Scanner(System.in);
        x=scanner.nextDouble();
        if((x>=0)&&(x<=3))F=pow(x,2);
        else F=4;
        System.out.println(F);
    }
}
