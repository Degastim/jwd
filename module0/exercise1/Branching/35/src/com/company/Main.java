package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	int day, month=1;
        int days[] = {31,28,31,30,31,30,31,31,30,31,30,31};
        Scanner scanner=new Scanner(System.in);
        day=scanner.nextInt();
        while (day > days[-1 + month])
        {
            day -= days[-1 + month++];
        }
        System.out.println("Month = " + month + ", day = " + day);
    }
}
