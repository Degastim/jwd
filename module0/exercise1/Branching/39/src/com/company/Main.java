package com.company;

import java.util.Scanner;
import static java.lang.Math.pow;
public class Main {

    public static void main(String[] args) {
        double x,F=0;
        Scanner scanner=new Scanner(System.in);
        x=scanner.nextDouble();
        if(x>=8)F=-x*x+x-9;
        else F=1/(pow(x,4)-6);
        System.out.println(F);
    }
}
