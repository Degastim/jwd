package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
	double x,y,t;
        Scanner scanner=new Scanner(System.in);
        x=scanner.nextDouble();
        y=scanner.nextDouble();
        if(x<y)
        {
            t=x;
            x=y;
            y=t;
        }
        System.out.println(x+" "+y);
    }
}
