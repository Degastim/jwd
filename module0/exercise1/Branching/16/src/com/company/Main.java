package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        double x,y;
        Scanner scanner=new Scanner(System.in);
        x=scanner.nextDouble();
        y=scanner.nextDouble();
        if(x==0) {
            System.out.println("OX");
            System.exit(0);
        }
        if(y==0)
        {
            System.out.println("OY");
            System.exit(0);
        }
        if((x>0)&&(y>0))System.out.println("1");
        if((x<0)&&(y>0))System.out.println("2");
        if((x<0)&&(y<0))System.out.println("3");
        if((x>0)&&(y<0))System.out.println("4");

    }
}
