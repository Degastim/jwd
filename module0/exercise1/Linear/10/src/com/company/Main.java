package com.company;

import java.util.Scanner;
import static java.lang.Math.*;
public class Main {

    public static void main(String[] args) {
        double x,y;
        Scanner scanner=new Scanner(System.in);
        x=scanner.nextDouble();
        y=scanner.nextDouble();
        System.out.println((sin(x)+cos(y))/(cos(x)-sin(y))*tan(x*y));
    }
}
