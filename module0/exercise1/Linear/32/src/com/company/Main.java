package com.company;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        double m,n,k,p,q,r,S,min,sec,hour;
        Scanner scanner=new Scanner(System.in);
        m=scanner.nextDouble();
        n=scanner.nextDouble();
        k=scanner.nextDouble();
        p=scanner.nextDouble();
        q=scanner.nextDouble();
        r=scanner.nextDouble();
        S=(m+p)*3600+(n+q)*60+(k+r);
        if (S>24*3600)
        {
            S=S-24*3600;
            System.out.print((S-S%3600)/3600);
            System.out.print("ч ");
            S=S%3600;
            System.out.print((S-S%60)/60);
            System.out.print("мин ");
            S=S%60;
            System.out.print(S);
            System.out.print("сек ");
        }
        else
            {
            sec=k+r;
            min=q+n;
            hour=m+p;
            if (sec > 60)
            {
                sec=sec-60;
                min++;
            }
            if (min>60)
            {
                hour++;
                min=min-60;
            }
            System.out.print(hour+"ч "+min+"мин "+sec+"с ");
            }
    }
}


