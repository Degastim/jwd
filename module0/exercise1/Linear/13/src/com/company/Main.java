package com.company;

import java.util.Scanner;
import static java.lang.Math.*;

public class Main {

    public static void main(String[] args)
    {
        double x1,y1,x2,y2,x3,y3,perimetr,area,side1,side2,side3,p;
        Scanner scanner=new Scanner(System.in);
        x1=scanner.nextDouble();
        y1=scanner.nextDouble();
        x2=scanner.nextDouble();
        y2=scanner.nextDouble();
        x3=scanner.nextDouble();
        y3=scanner.nextDouble();
        side1=sqrt(pow(abs(x2-x1),2)+pow(abs(y2-y1),2));
        side2=sqrt(pow(abs(x3-x1),2)+pow(abs(y3-y1),2));
        side3=sqrt(pow(abs(x2-x3),2)+pow(abs(y2-y3),2));
        perimetr=side1+side2+side3;
        p=perimetr/2;
        area=sqrt(p*(p-side1)*(p-side2)*(p-side3));
        System.out.println(perimetr);
        System.out.println(area);
    }
}
