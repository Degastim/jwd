package com.company;

import java.util.Scanner;
import static java.lang.Math.*;
public class Main {

    public static void main(String[] args) {
	double a,area,height,radiusin,radiusout;
        Scanner scanner=new Scanner(System.in);
        a=scanner.nextDouble();
        area=a*a*sqrt(3)/4;
        height=a*sqrt(3)/2;
        radiusin=sqrt(3)*a/6;
        radiusout=sqrt(3)*a/3;
        System.out.println(area);
        System.out.println(height);
        System.out.println(radiusin);
        System.out.println(radiusout);
    }
}
