package com.company;

import java.util.Scanner;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Main {

    public static void main(String[] args)
    {
	double b,a,c,result;
        Scanner scanner=new Scanner(System.in);
        b=scanner.nextInt();
        a=scanner.nextInt();
        c=scanner.nextInt();
        result=(b+sqrt(pow(b,2)+4*a*c))/(2*a)-pow(a,3)*c+pow(b,-2);
        System.out.println(result);
    }

}
