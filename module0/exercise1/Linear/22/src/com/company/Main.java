package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	int T;
        Scanner scanner=new Scanner(System.in);
        T=scanner.nextInt();
        System.out.print((T-T%3600)/3600);
        System.out.print("ч ");
        T=T%3600;
        System.out.print((T-T%60)/60);
        System.out.print("мин ");
        T=T%60;
        System.out.print(T);
        System.out.print("сек ");
    }
}
