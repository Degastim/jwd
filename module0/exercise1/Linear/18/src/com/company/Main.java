package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        double a,faceArea,totalArea,cubeVolume;
        Scanner scanner=new Scanner(System.in);
        a=scanner.nextDouble();
        faceArea=a*a;
        totalArea=faceArea*6;
        cubeVolume=a*a*a;
        System.out.println(faceArea);
        System.out.println(totalArea);
        System.out.println(cubeVolume);
    }
}
