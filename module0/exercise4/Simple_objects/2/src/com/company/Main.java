package com.company;

class Test2
{
    private double variable1;
    private double variable2;
    Test2(double variable1,double variable2)
    {
        this.variable1=variable1;
        this.variable2=variable2;
    }

    public double getVariable1() {
        return variable1;
    }

    public void setVariable1(double variable1) {
        this.variable1 = variable1;
    }

    public double getVariable2() {
        return variable2;
    }

    public void setVariable2(double variable2) {
        this.variable2 = variable2;
    }
}
public class Main
{
    public static void main(String[] args)
    {

    }
}
