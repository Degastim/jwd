package com.company;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;

class Train
{
    private String location;
    private int numberTrain;
    private Calendar deprtureTime;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getNumberTrain() {
        return numberTrain;
    }

    public void setNumberTrain(int numberTrain) {
        this.numberTrain = numberTrain;
    }

    public Calendar getDeprtureTime() {
        return deprtureTime;
    }

    public void setDeprtureTime(Calendar deprtureTime) {
        this.deprtureTime = deprtureTime;
    }
    Train(String location,int numberTrain,Calendar departureTime)
    {
        this.deprtureTime=departureTime;
        this.location=location;
        this.numberTrain=numberTrain;
    }
    public static void getInfo(Train[] trains,int numberTrain)
    {
        for (Train i:trains)
        {
            if (i.getNumberTrain()==numberTrain)
            {
                System.out.print(i.location+" "+ i.numberTrain+" "+i.deprtureTime.getTime());
            }
        }
    }
}
class TrainComparator
{

    public static Train[] compareByNumberTrain(Train[] arr) {
        Arrays.sort(arr, Comparator.comparingInt(Train::getNumberTrain));
        return arr;
    }

    public static Train[] compareByLocation(Train[] arr) {
        Arrays.sort(arr, Comparator.comparing(Train::getLocation).thenComparing(Train::getDeprtureTime));
        return arr;
    }
}
public class Main {

    public static void main(String[] args)
    {

    }
}
