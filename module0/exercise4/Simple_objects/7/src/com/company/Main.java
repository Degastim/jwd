package com.company;
import static java.lang.Math.sqrt;
import static java.lang.Math.pow;
class Triangle
{
    private double AB;
    private double BC;
    private double AC;
    private double Ax;
    private double Ay;
    private double Bx;
    private double By;
    private double Cx;
    private double Cy;
    public Triangle(double Ax,double Ay, double Bx,double By, double Cx, double Cy) \
    {
        this.Ax=Ax;
        this.Ay=Ay;
        this.Bx=Bx;
        this.By=By;
        this.Cx=Cx;
        this.Cy=Cy;
    }
    public void Side()
    {
        AB= sqrt(pow((Ax-Bx),2)+pow((By-Ay),2));
        BC= sqrt(pow((Cx-Bx),2)+pow((By-Cy),2));
        AC= sqrt(pow((Ax-Cx),2)+pow((Cy-Ay),2));
    }
    public double Perimeter()
    {
        Side();
        return AB+BC+AC;
    }
    public double Area()
    {
        double p=Perimeter()/2;
        return sqrt(p*(p-AB)*(p-BC)*(p-AC));
    }
    public void center()
    {
        double x=(Ax+Bx+Cx)/3;
        double y=(Ay+By+Cy)/3;
        System.out.print("x"+x+"\ny"+y);
    }
}
public class Main {

    public static void main(String[] args) {
	// write your code here
    }
}
