package com.company;




class Counter
{

    private int Value;
    private int lowLimit;
    private int highLimit;

    public int getValue()
    {
        return Value;
    }
    public Counter(int value, int lowLimit, int highLimit) {
        Value = value;
        this.lowLimit = lowLimit;
        this.highLimit = highLimit;
    }

    public int increaseValue()
    {
        if (Value < highLimit) {
            return Value++;
        }
        else return Value;
    }

    public int decreaseValue()
    {
        if (Value > lowLimit)
        {
            return Value--;
        }
        else return Value;
    }
    public void print()
    {

    }
}




public class Main {

    public static void main(String[] args)
    {

        Counter counter2 = new Counter(50, 10, 100);


        System.out.println("Значение счетчика 2: " + counter2.getValue());

        counter2.increaseValue();
        System.out.println(counter2.getValue());
        counter2.decreaseValue();

        System.out.println(counter2.getValue());

    }
}
