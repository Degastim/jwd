package com.company;
import java.util.Scanner;

class Student
{
    private String name;
    private long numberOfGroup;
    private int[] mark;

    public String getName()
    {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getNumberOfGroup() {
        return numberOfGroup;
    }

    public void setNumberOfGroup(long numberOfGroup) {
        this.numberOfGroup = numberOfGroup;
    }

    public int[] getMark()
    {
        return mark;
    }

    public void setMark(int[] mark)
    {
        this.mark = mark;
    }

    public Student(String name, long numberOfGroup, int[] mark)
    {
        this.name = name;
        this.numberOfGroup = numberOfGroup;
        this.mark = mark;
    }



}


public class Main
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        Student[] students = new Student[10];

        int k = 0;
        while(k<10)
        {
            System.out.println("Введите имя");
            String name = scanner.next();
            System.out.println("Введите номер группы");
            int group = scanner.nextInt();
            System.out.println("Введите оценки");
            int[] marks = new int[5];
            boolean good = true;
            for (int i = 0; i<5; i++)
            {
                marks[i] = scanner.nextInt();
                if (marks[i]<9)
                {
                    good = false;
                }
            }
            if (good)
            {
                Student student = new Student(name,group,marks);
                System.out.println("Неплох");
                students[k] = student;
                k++;
            }
        }
    }
}
