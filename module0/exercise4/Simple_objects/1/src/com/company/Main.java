package com.company;

class Test1
{
   private double variable1;
   private double variable2;
   Test1(double variable1,double variable2)
   {
       this.variable1=variable1;
       this.variable2=variable2;
   }
   void print()
   {
       System.out.print(variable1 + " " + variable2);
   }
   void changeVariable1(double variable1)
   {
       this.variable1=variable1;
   }
   void changeVariable2(double variable2)
   {
       this.variable2 = variable2;
   }
   double sum()
   {
       return variable1+variable2;
   }
   double max()
   {
       if(variable1>variable2)return variable1;
       else if(variable1<variable2)return variable2;
       else return variable1;
   }
}
public class Main
{
    public static void main(String[] args)
    {

        Test1 obj=new Test1(1,2);
        obj.print();
        obj.changeVariable1(3);
        obj.changeVariable2(4);
        System.out.print(obj.max());
        System.out.print(obj.sum());
        obj.print();
    }
}
