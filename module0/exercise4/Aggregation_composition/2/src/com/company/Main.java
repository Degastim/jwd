package com.company;
class Car
{
   private String mark;
   private int numberOfWheels;
   private boolean works;
    Car(String mark,Wheel wheel,Engine engine)
    {
        this.mark=mark;
        numberOfWheels=wheel.getValue();
        works=engine.launch();
    }
    void conclusion()
    {
        System.out.print(mark);
    }
    void run()
    {
     System.out.print("Поехали");
    }

}
class Wheel
{
    int number;
    Wheel(int number)
    {
       this.number=number;
    }
    void replacement(int number)
    {
        this.number=number;
    }
    int getValue()
    {
        return number;
    }
}
class Engine
{
    private boolean works;
    Engine(boolean works)
    {
        this.works=works;
    }
    boolean launch()
    {
        return works;
    }
    void refuel()
    {
        System.out.print("Заправился");
    }
}
public class Main {

    public static void main(String[] args)
    {
        Wheel wheel=new Wheel(4);
        Engine engine=new Engine(true);
        Car auto=new Car("Audi",wheel,engine);

    }
}
