package com.company;

import java.util.ArrayList;

class State
{
    private String name;
    State(String name)
    {
        this.name=name;
    }
    private ArrayList<Region> regions=new ArrayList<Region>();
    void addRegion(Region region)
    {
        regions.add(region);
    }
    void outCapital()
    {
        for (Region o:regions)
        {
            for (District e: o.districts)
            {
                for (Town n: e.towns)
                {
                    if(n.capital==true) System.out.print(n.name+"\n");
                }
            }
        }
    }
    void outNumberOfRegion()
    {
        int num=0;
        for (Region o:regions) num++;
        System.out.print(num+"\n");
    }
    void outSquare()
    {
        double square=0;
        for (Region o:regions)
        {
            square+=o.square;
        }
        System.out.print(square+"\n");
    }
    void outRegionalCenters()
    {
        for (Region o:regions)
        {
            for (District e: o.districts)
            {
                for (Town n: e.towns)
                {
                    if(n.regionalCenter==true) System.out.print(n.name+"\n");
                }
            }
        }
    }
}
class Region
{
    String name;
    double square=0;
    Region(String name)
    {
        this.name=name;
    }
    ArrayList<District> districts=new ArrayList<District>();
    void addDistrict(District district)
    {
        districts.add(district);
        for (District o : districts)
        {
            square+=o.square;
        }
    }

}

class District
{
    String name;
    double square;
    ArrayList<Town> towns=new ArrayList<Town>();
    District(String name,double square)
    {
        this.name=name;
        this.square=square;
    }
    void addTown(Town town)
    {
        towns.add(town);
    }
}
class Town
{
    String name;
    boolean capital;
    boolean regionalCenter;
    Town(String name,boolean capital,boolean regionalCenter)
    {
        this.capital=capital;
        this.regionalCenter=regionalCenter;
        this.name=name;
    }

}
public class Main
{
    public static void main(String[] args)
    {
        Town town1=new Town("Витебск",true,false);
        Town town2=new Town("Полоцк",false,true);
        District vitebsk=new District("Витебский район",353);
        vitebsk.addTown(town1);
        vitebsk.addTown(town2);
        Region VitebskRegion=new Region("Vitebsk");
        VitebskRegion.addDistrict(vitebsk);
        State Belarus=new State("Беларусь");
        Belarus.addRegion(VitebskRegion);
        Belarus.outCapital();
        Belarus.outNumberOfRegion();
        Belarus.outRegionalCenters();
        Belarus.outSquare();
    }
}
