package com.company;
class Sentence
{
    private String value="";
    public void addValue(Word word)
    {
        value += " " + word.getValue();
    }
    public String getValue()
    {
        return value;
    }
}
class Text {

    private String header;
    private String body = "";
    public Text(Word word)
    {
        header = word.getValue();
    }
    public Text(Sentence sentence)
    {
        header = sentence.getValue();
    }
    public String getHeader()
    {
        return header;
    }
    public void addBody(Word word)
    {
        body += " " + word.getValue();
    }
    public void addBody(Sentence sentence)
    {
        body += " " + sentence.getValue();
    }
    public String getBody()
    {
        return body;
    }
}
class Word
{
    private String word;
    public Word(String word)
    {
        this.word = word;
    }
    public String getValue()
    {
        return word;
    }
}
public class Main
{
    public static void main(String[] args)
    {
        Word word = new Word("abc");

        Text text = new Text(word);

        Word word1 = new Word("a");

        Word word2 = new Word("and");

        Word word3 = new Word("b");

        Sentence sentence = new Sentence();

        sentence .addValue(word1);

        sentence .addValue(word2);

        sentence .addValue(word3);

        text.addBody(sentence);

        System.out.println("Head: "+text.getHeader());

        System.out.println("Body: "+text.getBody());
    }
}
